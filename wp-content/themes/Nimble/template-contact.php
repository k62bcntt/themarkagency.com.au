<?php
/**
 * Template Name: Contact
 *
 */

get_header(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/contact-second.css'); ?>">
    <script src="<?php echo theme_uri('assets/scripts/origin/second-level.html') ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/twitter-second.js') ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/contact.js') ?>"></script>
	<div class="contact-page">
        <div class="wrapper">
            <div id="services-content-top">
                <div class="row">
                    <div class="left">
                        <p><?php echo $post->post_excerpt; ?></p>
                    </div>
                    <div class="enquiry-form" id="enquiry-form">
                        <p id="all-fields-required">
                            <?php 
                            if( ICL_LANGUAGE_CODE == 'vi') {
                                echo 'Tất cả các trường là bắt buộc';
                            } else {
                                echo 'All fields are required';
                            }
                            ?>
                        </p>
                        <fieldset>
                            <?php 
                            if(is_active_sidebar('contact')) {
                                dynamic_sidebar('contact');
                            }
                            ?>
                        </fieldset>
                    </div>
                    <br>
                    <div id="contact-information">
                        <?php 
                        if( ICL_LANGUAGE_CODE == 'vi') {
                            if(is_active_sidebar('footer-contact')) {
                                dynamic_sidebar('footer-contact');
                            }
                        } else {
                            if(is_active_sidebar('footer-contact-english')) {
                                dynamic_sidebar('footer-contact-english');
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
	</div>

<?php get_footer(); ?>

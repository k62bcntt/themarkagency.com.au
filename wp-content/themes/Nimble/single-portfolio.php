<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/styles.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/portfolio-base.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/our-work-second.css'); ?>">
    <script src="<?php echo theme_uri('assets/scripts/origin/twitter-second.js'); ?>"></script>
	<div id="single-portfolio-page" class="content-area">
		<div class="wrapper">
            <div id="services-content-top">
                <div class="row">
                    <div class="left">
                        <?php echo $post->post_content; ?>
                    </div>
                    <div class="right">
                        <?php 
                        echo get_field('action_tab_right', $post->ID);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="contact-the-mark-agency">
            <div class="wrapper">
                <?php 
                if(is_active_sidebar('footer-second')) {
                    dynamic_sidebar('footer-second');
                }
                ?>
            </div>
        </div>
	</div><!-- .content-area -->

<?php get_footer(); ?>

<?php 
/**
 * This file create Portfolio custom post type
 *
 */

namespace App\CustomPosts;

use App\Abstracts\CustomPost;

class PortfolioCustomPost extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'portfolio';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Portfolio';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Portfolio';

    /**
     * $args optional
     * @var  array
     */
    public $args = [
        'menu_icon' => 'dashicons-grid-view',
        'taxonomies' => ['post_tag', 'category']
    ];

}

<?php 
/**
 * This file create Service custom post type
 *
 */

namespace App\CustomPosts;

use App\Abstracts\CustomPost;

class ServiceCustomPost extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'service';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Service';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Services';

    /**
     * $args optional
     * @var  array
     */
    public $args = [
        'menu_icon' => 'dashicons-tickets',
        'taxonomies' => ['post_tag', 'category']
    ];

}

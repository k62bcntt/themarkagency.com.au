<?php 

namespace App;

class Manager
{
    /**
     * [$listen description]
     * @var array
     */
    public $listen = [
		\App\CustomPosts\PortfolioCustomPost::class,
		// \App\CustomPosts\ServiceCustomPost::class,
    ];

    /**
     * [__construct description]
     */
    public function __construct() {
        foreach ($this->listen as $class) {
            $this->resolveType($class);
        }
    }

    /**
     * [resolveCustomPost description]
     * @param  [type] $type [description]
     * @return [type]           [description]
     */
    public function resolveType($type)
    {
        return new $type();
    }
}
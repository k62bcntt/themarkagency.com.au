$(window).load(function(){
	var windowWidth	=	$(window).width();
	var windowHeight	=	$(window).height();
	if (windowWidth < 1315){
		$('#our-work .wrapper').css({'width':'1000px'});
	} else{
		$('#our-work .wrapper').css({'width':'1316px'});
		$('#our-work #portfolio-pieces').css({'width':'1316px'});
	}
    $('.port').hover(function(){
		$currentEl	=	$(this);
		$currentEl.find('.portfolio-info').fadeIn(0);
		$currentEl.find('p').stop(true,false).animate({'top':'50%'},450,'easeInOutBack');
	},function(){
		$currentEl	=	$(this);
		$currentEl.find('p').animate({'top':'125%'},450,'easeInOutBack');
		$currentEl.find('.portfolio-info').fadeOut(600);
	});

	$('.portfolio-info').each(function(){
		$(this).click(function(){
			var linkId	=	$(this).parent().find('a').attr('id');
			window.location = $('#'+linkId).attr('href');
			event.preventDefault();
		});
	});
});
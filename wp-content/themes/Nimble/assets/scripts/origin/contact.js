$(document).ready(function(){
	$('#name').focus(function(){
		if ($(this).val() == 'Name:'){
			$(this).val('');
		}
	});
	$('#name').blur(function(){
		if ($(this).val() == ''){
			$(this).val('Name:');
		}
	});
	
	$('#email').focus(function(){
		if ($(this).val() == 'Email:'){
			$(this).val('');
		}
	});
	$('#email').blur(function(){
		if ($(this).val() == ''){
			$(this).val('Email:');
		}
	});
	$('#phone').focus(function(){
		if ($(this).val() == 'Phone:'){
			$(this).val('');
		}
	});
	$('#phone').blur(function(){
		if ($(this).val() == ''){
			$(this).val('Phone:');
		}
	});
	$('#enquiry').focus(function(){
		if ($(this).val() == 'Enquiry:'){
			$(this).val('');
		}
	});
	$('#enquiry').blur(function(){
		if ($(this).val() == ''){
			$(this).val('Enquiry:');
		}
	});
});
function process_enquiry(){
  $("input").each(function(){
    $(this).removeClass("error");
  });
  $("textarea").removeClass("error");
  var error_message = "";
  var url_var = "";

  if($("#name").val()=="" || $("#name").val()=="Name:"){
    error_message = error_message + "Please fill in your name\n";
    $("#name").addClass("error");
  }
  if($("#phone").val()=="" || $("#phone").val()=="Phone:" ){
    error_message = error_message + "Please fill in phone number\n";
    $("#phone").addClass("error");
  }
  if($("#enquiry").val()=="" || $("#func_date").val()=="Date of Function*" ){
    error_message = error_message + "Please fill in the date of your function\n";
    $("#func_date").addClass("error");
  }
  if($("#email").val()=="" ){
    error_message = error_message + "Please fill in your email address\n";
    $("#email").addClass("error");
  } else if (!ValidateEmail($("#email").val())){
    error_message = error_message + "Please enter a valid email address\n";
    $("#email").addClass("error");
  }
  url_var += "?name="+$("#name").val();
  url_var += "&email="+$("#email").val();
  url_var += "&phone="+$("#phone").val();
  url_var += "&enquiry="+$("#enquiry").val();

  if(error_message!=""){
    //alert(error_message);
	$('#all-fields-required').addClass('error');
	setTimeout(function(){
		$('#all-fields-required').removeClass('error');
		$('input').each(function(){
			$(this).removeClass('error');
		});
		$('textarea').each(function(){
			$(this).removeClass('error');
		});
	},300);
    return false;
  }

  var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      //var res=xmlhttp.responseText;
      //document.getElementById("register").innerHTML=res;
      $("#enquiry-form").fadeOut(300,function(){
        $("#thankyou").fadeIn(300);
      })
    }
  }
  xmlhttp.open("GET.html","mail.html"+url_var,true);
  xmlhttp.send();
  return false;
}

function ValidateEmail(mail)
{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
		return true;
	}
	
	return false;
}
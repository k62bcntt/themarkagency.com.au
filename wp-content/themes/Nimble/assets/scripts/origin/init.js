$(document).ready(function(){
	if($('.homepage').length > 0) {
		set_size();
		$('#close-overlay').click(function(){
			$('#about-us-information').fadeOut(600);
			$('#us-image').fadeIn(900);
		});
		$('#us-image').click(function(){
			$('#us-image').fadeOut(600);
			$('#about-us-information').fadeIn(900);
		});
	//	fix_sizes();
		$(window).resize(function(){
			set_size();
		});
	}
});
$(window).load(function(){
//	fix_sizes();
});
function set_size(){
	var windowWidth		=	$(window).width();
	var windowHeight	=	$(window).height();
	
	
	var recentWorksHeight	=	windowWidth*0.30859375;
	$('#recent-works').css({'height':recentWorksHeight+'px'});
	
	var aboutTopPosition	=	$('#recent-works').height()+84+40;
	$('#about-the-mark-agency').css({'top':aboutTopPosition+'px'});
	
	var workTopPosition	=	$('#about-the-mark-agency').offset().top + ($(window).height());
	$('#our-work').css({'top':workTopPosition+'px'});
	
	var contactTopPosition	=	$('#about-the-mark-agency').offset().top + ($(window).height()*2);
	$('#contact-the-mark-agency').css({'top':contactTopPosition+'px'});
	
	
	var current_ratio = $(window).width() / $(window).height();
	var bg_dimension_ratio = 2560 / 1440;
  	if (current_ratio < bg_dimension_ratio){
		$('#about-the-mark-agency').css({'background-size':'auto '+$(window).height()+'px'});
	} else {
		$('#about-the-mark-agency').css({'background-size':$(window).width()+'px auto'});
	}
	$('#scrollable').css({'width':$(window).width(),'margin':'0px auto 0px auto'});
}
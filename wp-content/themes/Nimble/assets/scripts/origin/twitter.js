$(document).ready(function(){
	var windowWidth	=	$(window).width();
    $.get('ajax/twitter75eb.html?taccount=themarkagency&amp;tcount=4', function(data) {
    $('.twitterfeed').html(data);
	if (windowWidth < 1195 ){
		// Only load one tweet
		$('.tweet_two').fadeIn(600).css({'display':'inline-block'});
	} else{
		if (windowWidth < 1713){
			// Load two tweets
			$('.tweet_two').fadeIn(600).css({'display':'inline-block'});
			setTimeout(function(){
				$('.tweet_two').animate({'left':'470px'},900,'easeInOutQuad',function(){
					$('.tweet_one').fadeIn(600).css({'display':'inline-block'}).addClass('first');
					$('.tweet_two').css({'left':'0px'}).removeClass('first');
				});
			},5000);
		} else{
			if (windowWidth < 2247){
				// Load three tweets
				$('.tweet_two').fadeIn(600).css({'display':'inline-block'});
				$('.tweet_three').fadeIn(600).css({'display':'inline-block'});
				
				setTimeout(function(){
					$('.tweet_two').animate({'left':'490px'},900,'easeInOutQuad',function(){
						$('.tweet_one').fadeIn(600).css({'display':'inline-block'}).addClass('first');
						$('.tweet_two').css({'left':'0px'}).removeClass('first');
					});
					$('.tweet_three').animate({'left':'490px'},900,'easeInOutQuad',function(){
						$('.tweet_three').css({'left':'0px'});
					});
				},5000);
				
			} else{
				// Load four tweets
				$('.tweet_two').fadeIn(600).css({'display':'inline-block'});
				$('.tweet_three').fadeIn(600).css({'display':'inline-block'});
				$('.tweet_four').fadeIn(600).css({'display':'inline-block'});
				setTimeout(function(){
					$('.tweet_two').animate({'left':'490px'},900,'easeInOutQuad',function(){
						$('.tweet_one').fadeIn(600).css({'display':'inline-block'}).addClass('first');
						$('.tweet_two').css({'left':'0px'}).removeClass('first');
					});
					$('.tweet_three').animate({'left':'490px'},900,'easeInOutQuad',function(){
						$('.tweet_three').css({'left':'0px'});
					});
					$('.tweet_four').animate({'left':'490px'},900,'easeInOutQuad',function(){
						$('.tweet_four').css({'left':'0px'});
					});
				},5000);
			}
		}
	}
  }, 'html');
});
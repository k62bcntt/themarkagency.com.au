$(window).resize(function(){
//	position_discs();
});
$(window).load(function(){
	position_discs();
});
function position_discs(){
	var windowWidth	=	$(window).width();
	
    if ($('html').hasClass('csstransforms3d')) {	
		$('.thumb').removeClass('scroll').addClass('flip');		
		$('.thumb.flip').hover(function () {
			$(this).find('.thumb-wrapper').addClass('flipIt');
		});
		$('.thumb.flip').hover(function(){},function () {
			$(this).find('.thumb-wrapper').removeClass('flipIt');			
		});
	} else {
		$('.thumb').hover(function () {
			$(this).find('.thumb-detail').stop().animate({bottom:0}, 10, 'easeOutCubic');
		},function () {
			$(this).find('.thumb-detail').stop().animate({bottom: ($(this).height() * -1) }, 10, 'easeOutCubic');			
		});
	}
	
	$('.disc').each(function(){
		var discWidth	=	$(this).width();
		var discHeight	=	$(this).height();
		var newWidth	=	windowWidth/2560*discWidth;
		var newHeight	=	newWidth;
		$(this).css({'height':newHeight+'px','width':newWidth+'px'});
		if ($(this).hasClass('black')){
			$(this).parent().parent().parent().css({'height':newHeight+'px','width':newWidth+'px'});
			$(this).parent().parent().parent().find('.thumb').css({'height':newHeight+'px','width':newWidth+'px'});
		}
	});
	
	var newBottom	=	windowWidth*0.33046875*0.7207368421;
	$('.process-step-one').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.3845148741;
	$('.process-step-two').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.4071510297;
	$('.process-step-three').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.5106407323;
	$('.process-step-four').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.5554050343;
	$('.process-step-five').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.01;
	$('.process-step-six').css({'bottom':newBottom+'px'});

	var newBottom	=	windowWidth*0.33046875*0.3750970252;
	$('.process-step-seven').css({'bottom':newBottom+'px'});
}
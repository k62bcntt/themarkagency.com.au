<?php

if (!function_exists('asset')) {
    /**
     * Get resource uri
     * @param string
     */
    function asset($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/assets/dist/' . $assets);
    }
}

if (!function_exists('asset_r')) {
    /**
     * Get resource uri
     * @param string
     */
    function asset_r($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/resources/' . $assets);
    }
}

if (!function_exists('theme_uri')) {
    /**
     * Get resource uri
     * @param string
     */
    function theme_uri($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/' . $assets);
    }
}

if (!function_exists('title')) {
    /**
     * Generate page title
     *
     * @return string
     */
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name . ' - ' . get_bloginfo('name');
        }

        if (is_404()) {
            return '404 page not found - ' . get_bloginfo('name');
        }

        return get_the_title() . ' - ' . get_bloginfo('name');
    }
}

if (!function_exists('createExcerptFromContent')) {
    /**
     * this function will create an excerpt from post content
     *
     * @param  string $content
     * @param  int    $limit
     * @param  string $readmore
     * @since  1.0.0
     * @return string $excerpt
     */
    function createExcerptFromContent($content, $limit = 50, $readmore = '...')
    {
        if (!is_string($content)) {
            throw new Exception("first parameter must be a string.");
        }

        if ($content == '') {
            throw new Exception("first parameter is not empty.");
        }

        if (!is_int($limit)) {
            throw new Exception("second parameter must be the number.");
        }

        if ($limit <= 0) {
            throw new Exception("second parameter must greater than 0.");
        }

        $words = explode(' ', $content);

        if (count($words) <= $limit) {
            $excerpt = $words;
        } else {
            $excerpt = array_chunk($words, $limit)[0];
        }

        return strip_tags(implode(' ', $excerpt)) . $readmore;
    }
}

if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? '' : $img[0];
    }
}

if (!function_exists('garung_paginate')) {
    function garung_paginate($paged, $total_pages) {
        ?>
        <div class="row paginate-section">
            <div class='paginate text-center'>
                <?php
                if ($total_pages > 1) {
                    $current_page = max(1, $paged);
                    echo paginate_links(array(
                        'base' => @add_query_arg('trang', '%#%'),
                        'format' => '?trang=%#%',
                        'current' => $current_page,
                        'total' => $total_pages,
                        'next_text' => __('Trang sau') . ' »',
                        'prev_text' => '« ' . __('Trang trước'),
                    ));
                }
                ?>
            </div>
        </div>
    <?php
    }
}
add_action('garung_paginate', 'garung_paginate', 10, 2);
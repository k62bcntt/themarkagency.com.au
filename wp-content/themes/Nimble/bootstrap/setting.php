<?php 
/**
* Setting theme
*/
class Setting
{
	public function __construct()
	{
		add_action('widgets_init', [$this, 'themeSidebars']);
        add_action('after_setup_theme', [$this, 'settingLogo']);
        add_action('after_setup_theme', [$this, 'menuSupport']);
        add_action('init', [$this, 'addSupport']);
	}

    public function addSupport() {
        add_post_type_support('page', 'excerpt');
        add_post_type_support('product', 'post-formats');
        add_theme_support('post-formats', array('aside', 'gallery'));
        add_theme_support('gallery', array('page', 'product'));
    }
    public function settingLogo() {
        $defaults = array(
            'height' => 160,
            'width' => 60,
            'flex-height' => true,
            'flex-width' => true,
            'header-text' => array('site-title', 'site-description'),
        );
        add_theme_support('custom-logo', $defaults);
    }

    public function menuSupport() {
        register_nav_menus(array(
            'main-menu' => __('Main Menu', 'textdomain'),
        ));
    }

	public function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Main Sidebar', 'garung'),
                'id'            => 'main-sidebar',
                'description'   => __('Main Sidebar', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Contact Page', 'garung'),
                'id'            => 'contact',
                'description'   => __('Trang Liên Hệ', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer on Contact Page', 'garung'),
                'id'            => 'footer-contact',
                'description'   => __('Footer on Contact Page', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer on Contact Page - English', 'garung'),
                'id'            => 'footer-contact-english',
                'description'   => __('Footer on Contact Page - English', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer second ', 'garung'),
                'id'            => 'footer-second',
                'description'   => __('Footer on single and services Page', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer second - English', 'garung'),
                'id'            => 'footer-second-english',
                'description'   => __('Footer on single and services Page - English', 'garung'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
}

new Setting();
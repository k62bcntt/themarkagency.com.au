<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');
// add_action('admin_head', 'theme_enqueue_scripts');
// add_action('admin_head', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    // wp_enqueue_style(
    //     'template-style',
    //     asset('app.css'),
    //     false
    // );

    wp_enqueue_style(
        'font-theme',
        'http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700',
        false
    );
}

function theme_enqueue_scripts()
{
    // wp_enqueue_script(
    //     'template-scripts',
    //     asset('app.js'),
    //     'jquery',
    //     '1.0',
    //     true
    // );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
        'ajax_url' => admin_url('admin-ajax.php', $protocol),
    );
    // wp_localize_script('template-scripts', 'ajax_obj', $params);
}
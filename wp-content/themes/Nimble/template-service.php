<?php
/**
 * Template Name: Services
 *
 */
get_header(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/second-level.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/twitter-second.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/services.css'); ?>">
	<div class="service-page">
        <div class="wrapper">
            <div id="services-content-top">
                <div class="row">
                    <div class="left">
                        <p><?php echo get_the_title($post->ID); ?></p>
                    </div>
                    <div class="right">
                        <p><?php echo get_the_excerpt($post->ID); ?></p>
                    </div>
                </div>
            </div>
            <div id="services-content-bottom">
                <div class="row">
                    <?php echo $post->post_content; ?>
                </div>
            </div>
        </div>
        <div id="contact-the-mark-agency">
            <div class="wrapper">
                <?php 
                if(is_active_sidebar('footer-second')) {
                    dynamic_sidebar('footer-second');
                }
                ?>
            </div>
        </div>
	</div>

<?php get_footer(); ?>

<?php
/**
 * Template Name: Our Team
 *
 */

get_header();?>
    {{-- <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/contact-second.css'); ?>"> --}}
    <script src="<?php echo theme_uri('assets/scripts/origin/second-level.html') ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/twitter-second.js') ?>"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{-- <script src="<?php echo theme_uri('assets/scripts/origin/contact.js') ?>"></script> --}}
    <div class="ourteam-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-12">
                    <table id="tablepress-1" class="table table-bordered table-hover">
                        <thead>
                            <tr class="row-1 odd">
                                <th class="column-1">Name</th>
                                <th class="column-2">Profile</th>
                            </tr>
                        </thead>
                        <tbody class="row-hover">
                            <tr class="row-2 even">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/harry.jpg" alt="" width="295" height="300" class="alignnone size-medium wp-image-203">
                                    <br> Harry Hoang MIPA AFA MAICD | Founder &amp; CEO at Tailored Accounts</td>
                                <td class="column-2">Since 2005, Harry has understood the importance and impact of technology in accounting. His solution of cloud-based accounting could eliminate non-value adding activities such as the manual, endless process of invoicing, billing and receipt management. Today, Harry and his team have developed expertise in assisting more than 200 Australian businesses with the efficient management system through cloud-based accounting.</td>
                            </tr>
                            <tr class="row-3 odd">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Fuzuki.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Fuzuki Nishimura CPA Australia | Chief Technical Officer at Tailored Accounts</td>
                                <td class="column-2">Fuzuki looks after Tailored Accounts internal quality control system, Quality Management standard, and AccountantChange training programs. Fuzuki enjoys researching and training for junior accountants. She hopes to bring the best knowledge and skills for participants in AccountantChange.</td>
                            </tr>
                            <tr class="row-4 even">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Chien.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Chien Dang CPA Australia| Team Leader at Tailored Accounts</td>
                                <td class="column-2">Chien specialises in budgeting, cashflow management and economics modelling. His knowledge in Finance and Accounting has enabled him to generate valuable advice for clients. He is interested in technology and innovation in Accounting industry. He would like to share with experience with others, especially Accounting students and graduates.</td>
                            </tr>
                            <tr class="row-5 odd">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Teddy.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Teddy Nam| Senior Accountant at Tailored Accounts</td>
                                <td class="column-2">What I really enjoy about working at Tailored Accounts is that it not only provides a supportive and friendly work environment, but also allows me to apply what I have learnt in university to practice. I am committed to lifelong learning, and hope to continue growing with Tailored Accounts. I am optimistic about the future of Tailored Accounts, and I look forward to working with more of our clients.</td>
                            </tr>
                            <tr class="row-6 even">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Mike.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Mike Carmody | Principal at Paratus Global Consulting</td>
                                <td class="column-2">Mike is a seasoned director, chief executive and internationally recognised government advocate, business leader and strategist in the corporate, not-for-profit, and international peak body sectors. As a Director and CEO Mike has managed two global service organisations and two national industry peak bodies. Mike is degree qualified and a graduate of the Australian Army Officer Cadet Academy and Senior Officers’ Command and Staff College, and the Canadian Defence Force (NATO) Command College.</td>
                            </tr>
                            <tr class="row-7 odd">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Vikas.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Vikas Pandit| Head of Cyber Education at QILA</td>
                                <td class="column-2">Vikas is a senior learning and development professional with over two decades of professional training experience. His domain expertise is in E-Commerce, Telecom (Retail, Customer Care), Software, Business Process Outsourcing and IT Training. He has nearly a decade of experience in building and managing the training function across locations in top fortune 100 mobile phone manufacturing companies namely Samsung &amp; Nokia. Vikas is an entrepreneur at heart and in action and is passionate about building from the ground up.</td>
                            </tr>
                            <tr class="row-8 even">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Erin.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Erin Adams CPA Australia | Senior Account Manager at Xero</td>
                                <td class="column-2">My personal goal is to make business better and easier in regional areas while utilizing all the latest technology and best practices. A strategic thinker, I am equally at home working independently or contributing to a team. My communication, presentation and interpersonal skills are first rate and my hunger to learn sees me striving to reach new goals, both personal and professional.</td>
                            </tr>
                            <tr class="row-9 odd">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/David.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> David Hocking | Principal/Director at Strategic Fusion Group</td>
                                <td class="column-2">I have been involved in the Association sector for more than 20 years. Much of my success has been grounded in my years as a sports coach where I was an effective communicator and motivator of national and international athletes. I have effectively applied many of these skills to the business environment.</td>
                            </tr>
                            <tr class="row-10 even">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/John.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> John Forbes | CEO &amp; Founder at Civillise.ai</td>
                                <td class="column-2">John is Director and a Founder of Canberra based start-up Civilise.ai and is soon to graduate from a Master of Computing in Artificial Intelligence from ANU. His professional experience has included time in finance, healthcare, academia, mining, and telecommunications design and construction. He also holds a Master Degree in Engineering Management, and a Bachelor Degree in Mechanical and Mechatronic Engineering from UTS.</td>
                            </tr>
                            <tr class="row-11 odd">
                                <td class="column-1"><img src="http://accountantchange.com.au/wp-content/uploads/2018/03/Michael.png" alt="" class="alignnone size-medium wp-image-203">
                                    <br> Michael Konarzewski | Partner at Jigsaw Tax and Advisory</td>
                                <td class="column-2">My unique knowledge of the ATO’s processes and legislative interpretation methods has helped clients manage their ATO relationships to achieve better results. I provide advice on debt management, objections, technical requests for penalty/interest remissions, and overall ATO strategies. I like coming up with outcomes that are legal, high quality and add value to the client’s situation.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>
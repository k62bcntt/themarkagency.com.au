<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php 
$args = [
    'post_type' => 'portfolio',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'portfolio',
        ),
    ),
];

$portfolios = new WP_Query($args);
?>
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/styles.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/services.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/our-work-second.css'); ?>">
    <script src="<?php echo theme_uri('assets/scripts/origin/jquery.js'); ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/jquery.easing.1.3.js'); ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/ourwork-second.js'); ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/twitter-second.js'); ?>"></script>
    <div class="portfolio-page">
        <div id="our-work">
            <div class="wrapper">
                <div class="title">
                    <h1>OUR WORK</h1>
                </div>
                <div id="portfolio-pieces">
                    <?php 
                    if(!empty($portfolios->posts)):
                        foreach ($portfolios->posts as $key => $item) :
                            $link_item = get_permalink($item->ID);
                            $img = getPostImage($item->ID);
                    ?>
                        <div class="port">
                            <a id="portfolio-piece-44" href="<?php echo $link_item; ?>"><img src="<?php echo $img; ?>" width="305" height="167"></a>
                            <div class="portfolio-info">
                                <div class="portfolio-overlay" rel="portfolio-piece-44"></div>
                                <p><?php echo $item->post_title; ?>
                                    <br><span class="portsmall"><?php echo $item->post_excerpt; ?></span> </p>
                            </div>
                        </div>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div id="contact-the-mark-agency">
            <div class="wrapper">
                <?php 
                if(is_active_sidebar('footer-second')) {
                    dynamic_sidebar('footer-second');
                }
                ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
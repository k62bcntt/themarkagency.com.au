<?php 
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* e.g., it puts together the home page when no home.php file exists.
*
* Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
get_header(); 

$args_port = [
    'post_type' => 'portfolio',
    'post_status' => 'publish',
    'posts_per_page' => 20
];

$url = site_url("/wp-content/uploads/2018/04");

$get_portfolio = new WP_Query( $args_port );

$args_port_our_work_section = [
    'post_type' => 'portfolio',
    'post_status' => 'publish',
    'posts_per_page' => 8,
    'meta_query'  => [
        'key'     => 'show_on_homepage_our_work_section',
        'value'   => 'yes',
        'compare' => '=',
    ]
];

$portfolios_ourwork_section = new WP_Query( $args_port_our_work_section );
$portfolios_ourwork = $portfolios_ourwork_section->posts;

?>
<!-- <script src="<?php //echo theme_uri('assets/scripts/origin/scrollto.html'); ?>"></script> -->
<script src="<?php echo theme_uri('assets/scripts/origin/mousewheel.js'); ?>"></script>
<script src="<?php echo theme_uri('assets/scripts/origin/init.js'); ?>"></script>
<!-- <script src="<?php //echo theme_uri('assets/scripts/origin/scroll.html'); ?>"></script> -->
<script src="<?php echo theme_uri('assets/scripts/origin/custom.js'); ?>"></script>
<script src="<?php echo theme_uri('assets/scripts/origin/twitter.js'); ?>"></script>
<script src="<?php echo theme_uri('assets/scripts/origin/process.js'); ?>"></script>
<script src="<?php echo theme_uri('assets/scripts/origin/ourwork.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/services.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/our-work.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/contact.css'); ?>">
<div class="homepage">
    <div id="recent-works">
        <?php 
        if(!empty($get_portfolio->posts)):
            $portfolios = $get_portfolio->posts;
        ?>
        <div class="left">
            <?php 
            if(!empty($portfolios[0])):
            ?>
            <div class="left-top">
                <a href="<?php echo get_permalink($portfolios[0]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[0]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
            <?php 
            if(!empty($portfolios[1])):
            ?>
            <div class="left-left">
                <a href="<?php echo get_permalink($portfolios[1]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[1]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
            <?php 
            if(!empty($portfolios[2])):
            ?>
            <div class="left-right">
                <a href="<?php echo get_permalink($portfolios[2]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[2]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
        </div>
        <div class="middle-left">
            <?php 
            if(!empty($portfolios[3])):
            ?>
            <div class="middle-left-top">
                <a href="<?php echo get_permalink($portfolios[3]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[3]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
            <?php 
            if(!empty($portfolios[4])):
            ?>
            <div class="middle-left-bottom">
                <a href="<?php echo get_permalink($portfolios[4]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[4]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
        </div>
        <?php 
            if(!empty($portfolios[5])):
            ?>
        <div class="middle">
            <a href="<?php echo get_permalink($portfolios[5]->ID); ?>" class="recent-works-link">
                <div class="image-overlay"></div>
                <img src="<?php echo (getPostImage($portfolios[5]->ID)); ?>" />
            </a>
        </div>
        <?php endif; ?>
        <div class="grid">
            <?php 
            if(!empty($portfolios[6])):
            ?>
            <div class="grid-left-top">
                <a href="<?php echo get_permalink($portfolios[6]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[6]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
            <?php 
            if(!empty($portfolios[7])):
            ?>
            <div class="grid-left-bottom">
                <a href="<?php echo get_permalink($portfolios[7]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[7]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
        </div>
        <div class="grid">
            <?php 
            if(!empty($portfolios[8])):
            ?>
            <div class="grid-right-top">
                <a href="<?php echo get_permalink($portfolios[8]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[8]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
            <?php 
            if(!empty($portfolios[9])):
            ?>
            <div class="grid-right-bottom">
                <a href="<?php echo get_permalink($portfolios[9]->ID); ?>" class="recent-works-link">
                    <div class="image-overlay"></div>
                    <img src="<?php echo (getPostImage($portfolios[9]->ID)); ?>" />
                </a>
            </div>
            <?php endif; ?>
        </div>
        <?php 
        if(!empty($portfolios[10])):
        ?>
        <div class="right">
            <a href="<?php echo get_permalink($portfolios[10]->ID); ?>" class="recent-works-link">
                <div class="image-overlay"></div>
                <img src="<?php echo (getPostImage($portfolios[10]->ID)); ?>" />
            </a>
        </div>
        <?php endif; ?>
    </div>
    <div id="about-the-mark-agency">
        <div id="about-us-information">
            <div id="close-overlay">
                <h1>X</h1></div>
            <?php 
                echo get_the_content();
            ?>
            <h1>US.</h1>
            <p>The Mark Agency is a full-service marketing agency established in 2006. We specialise in marketing to solve business challenges. We like to identify the current challenges of the business and how marketing will solve those challenges within a strategic and measurable framework.</p>
            <p>Our aim is to continuously add value to your brand, your customers and your stakeholders. We specialise in the integration of traditional and new media. We judge our work, not only by how it looks, but how it helps build meaningful brands and lasting connections.</p>
            <p>Our in-house team of strategic, creative, and technical experts thrive on developing marketing and business solutions that only present themselves once the challenge has been analysed from our unique perspective.</p>
        </div>
    </div>
    <div id="our-work">
        <div class="wrapper">
            <div class="title">
                <h1>OUR WORK</h1>
            </div>
            <div id="portfolio-pieces">
                <?php 
                if(!empty($portfolios_ourwork[0])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[0]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[0]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[0]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[0]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[1])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[1]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[1]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[1]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[1]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[2])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[2]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[2]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[2]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[2]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[3])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[3]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[3]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[3]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[3]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[4])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[4]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[4]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[4]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[4]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[5])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[5]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[5]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[5]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[5]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[6])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[6]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[6]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[6]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[6]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php 
                if(!empty($portfolios_ourwork[7])):
                ?>
                <div class="port">
                    <a id="portfolio-piece-1" href="<?php echo get_permalink($portfolios_ourwork[7]->ID); ?>" class="recent-works-link">
                        <img src="<?php echo (getPostImage($portfolios_ourwork[7]->ID)); ?>" width="305" height="167"></a>
                    <div class="portfolio-info">
                        <div class="portfolio-overlay" rel="portfolio-piece-1"></div>
                        <p><?php echo $portfolios_ourwork[7]->post_title; ?>
                            <br><span class="portsmall"><?php echo $portfolios_ourwork[7]->post_excerpt; ?></span> </p>
                    </div>
                </div>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="view-our-portfolio">
                <?php 
                if( ICL_LANGUAGE_CODE == 'vi'):
                ?>
                <a href="<?php echo site_url('chuyen-muc/dich-vu') ?>"># XEM DANH MỤC CỦA CHÚNG TÔI</a>
                <?php 
                else:
                ?>
                <a href="<?php echo site_url('category/portfolio') ?>"># VIEW OUR PORTFOLIO</a>
                <?php  
                endif;
                ?>
            </div>
        </div>
    </div>
    <div id="contact-the-mark-agency" >
        <div class="wrapper">
            <?php 
            if( ICL_LANGUAGE_CODE == 'vi'):
            ?>
            <div class="left">
                <p>Interested in finding out how The Mark Agency can help you?
                <br /><span>Liên hệ với chúng tôi.</span></p>
            </div>
            <div id="contact-information">
                <p>PHONE: +61 2 6262 2011</p>
                <p>FACSIMILE: +61 2 6257 6641</p>
                <p>EMAIL: <a href="mailto:info@themarkagency.com.au">info@themarkagency.com.au</a></p>
                <p>POSTAL: PO BOX 707 Dickson ACT 2602</p>
                <p>LINKS:</p>
                <p><a href="https://www.hightail.com/dropbox?dropbox=The-Mark-Agency">YouSendIt</a></p>
                <p><a href="http://download.teamviewer.com/download/TeamViewerQS_en.exe">Team Viewer for Windows</a></p>
                <p><a href="http://download.teamviewer.com/download/TeamViewerQS.dmg">Team Viewer for Mac</a></p>
            </div>
            <?php 
            else:
            ?>
            <div class="left">
                <p>Interested in finding out how The Mark Agency can help you?
                <br /><span>Contact us today.</span></p>
            </div>
            <div id="contact-information">
                <p>PHONE: +61 2 6262 2011</p>
                <p>FACSIMILE: +61 2 6257 6641</p>
                <p>EMAIL: <a href="mailto:info@themarkagency.com.au">info@themarkagency.com.au</a></p>
                <p>POSTAL: PO BOX 707 Dickson ACT 2602</p>
                <p>LINKS:</p>
                <p><a href="https://www.hightail.com/dropbox?dropbox=The-Mark-Agency">YouSendIt</a></p>
                <p><a href="http://download.teamviewer.com/download/TeamViewerQS_en.exe">Team Viewer for Windows</a></p>
                <p><a href="http://download.teamviewer.com/download/TeamViewerQS.dmg">Team Viewer for Mac</a></p>
            </div>
            <?php  
            endif;
            ?>
        </div>
    </div>
</div>
<?php 
get_footer();
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- <meta name="viewport" content="width=device-width"> -->
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/dist/app.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo theme_uri('assets/styles/origin/styles.css'); ?>">
    <script src="<?php echo theme_uri('assets/scripts/origin/jquery.js'); ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/jquery.easing.1.3.js'); ?>"></script>
    <?php wp_head(); ?>
</head>
<body>
	<div id="scrollable">
        <div id="header">
            <div id="the-mark-logo">
            	<?php 
            	$custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                    $logo_url = esc_url( $logo[0] );
                } else {
                    $logo_url = 'http://fakeimg.pl/160x60';
                }
            	?>
                <a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_url; ?>" width="160" height="63" /></a>
            </div>
            <div id="navigation">
            	<?php
            	if (has_nav_menu('main-menu')):
                    echo wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']);
                endif;
            	?>
            </div>
        </div>
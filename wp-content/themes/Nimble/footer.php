<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Acountantchange
 * @since 1.0
 */
?>	
	</div>
	<div id="side-navigation">
        <a class="go-to-header active" id="header-link" href="javascript:void(0)"></a>
        <a class="go-to-about" id="about-link" href="javascript:void(0)"></a>
        <a class="go-to-work" id="work-link" href="javascript:void(0)"></a>
        <a class="go-to-contact" id="contact-link" href="javascript:void(0)"></a>
    </div>
	<?php wp_footer(); ?>
	<script src="<?php echo theme_uri('assets/scripts/origin/jquery.js'); ?>"></script>
    <script src="<?php echo theme_uri('assets/scripts/origin/jquery.easing.1.3.js'); ?>"></script>
</body>
</html>